﻿using TestCoverageDemo;
using Xunit;

namespace TestCoverage.Tests;

public class PrimeServiceTests
{
    readonly PrimeService _primeService;

    public PrimeServiceTests() => _primeService = new PrimeService();

    [Theory]
    [InlineData(-1), InlineData(0), InlineData(1)]
    public void IsPrime_ValuesLessThan2_ReturnFalse(int value) =>
        Assert.False(_primeService.IsPrime(value), $"{value} should not be prime");

    [Theory]
    [InlineData(2), InlineData(3), InlineData(5), InlineData(7)]
    public void IsPrime_PrimesLessThan10_ReturnTrue(int value) =>
        Assert.True(_primeService.IsPrime(value), $"{value} should be prime");
}